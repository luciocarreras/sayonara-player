/* GuiUtils.cpp */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* GuiUtils.cpp */

#include "GuiUtils.h"

#include <QApplication>
#include <QCache>
#include <QDesktopWidget>
#include <QDirIterator>
#include <QFontMetrics>
#include <QIcon>
#include <QList>
#include <QPalette>
#include <QPixmap>
#include <QRegExp>
#include <QScreen>
#include <QSize>
#include <QString>

#include <algorithm>

namespace Gui
{
	QColor Util::color(QPalette::ColorGroup colorGroup, QPalette::ColorRole colorRole)
	{
		return QApplication::palette().color(colorGroup, colorRole);
	}

	QScreen* Util::getBiggestScreen()
	{
		const auto screens = QApplication::screens();
		const auto it = std::max_element(screens.cbegin(),
		                                 screens.cend(),
		                                 [](const auto* screen1, const auto* screen2) {
			                                 return (screen1->size().height() < screen2->size().height());
		                                 });

		return (it != screens.end())
		       ? *it
		       : nullptr;
	}

	void Util::placeInScreenCenter(QWidget* widget, float relativeSizeX, float relativeSizeY)
	{
		const auto* screen = getBiggestScreen();
		if(!screen)
		{
			return;
		}

		const auto width = screen->size().width();
		const auto height = screen->size().height();

		if(relativeSizeX < 0.1f || relativeSizeY < 0.1f)
		{
			relativeSizeX = 0.7f;
			relativeSizeY = 0.7f;
		}

		const auto xRemainder = (1.0f - relativeSizeX) / 2.0f;
		const auto yRemainder = (1.0f - relativeSizeY) / 2.0f;

		const auto xAbs = std::max(0, static_cast<int>(width * xRemainder)) + screen->geometry().x();
		const auto yAbs = std::max(0, static_cast<int>(height * yRemainder)) + screen->geometry().y();
		auto wAbs = std::max(0, static_cast<int>(width * relativeSizeX));
		auto hAbs = std::max(0, static_cast<int>(height * relativeSizeY));

		if(wAbs == 0)
		{
			wAbs = 1200;
		}

		if(hAbs == 0)
		{
			hAbs = 800;
		}

		widget->setGeometry(xAbs, yAbs, wAbs, hAbs);
	}

	int Util::textWidth(const QFontMetrics& fm, const QString& text)
	{
#if QT_VERSION_MAJOR >= 5 && QT_VERSION_MINOR >= 11
		return fm.horizontalAdvance(text);
#else
		return fm.width(text);
#endif
	}

	int Util::textWidth(QWidget* widget, const QString& text)
	{
		return textWidth(widget->fontMetrics(), text);
	}

	int Util::viewRowHeight(const QFontMetrics& fontMetrics)
	{
		constexpr const auto offset = 6;
		return fontMetrics.height() + offset;
	}
}