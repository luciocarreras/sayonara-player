/* ContextMenu.h */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONTEXTMENU_H
#define CONTEXTMENU_H

#include "Gui/Utils/Widgets/WidgetTemplate.h"
#include "Utils/Pimpl.h"

#include <QMenu>

namespace Gui
{
	class PreferenceAction;
	using ContextMenuEntries = uint16_t;

	class ContextMenu :
		public Gui::WidgetTemplate<QMenu>
	{
		Q_OBJECT
		PIMPL(ContextMenu)

		public:
			enum Entry
			{
				EntryNone = 0,
				EntryNew = (1 << 0),
				EntryEdit = (1 << 1),
				EntryUndo = (1 << 2),
				EntrySave = (1 << 3),
				EntrySaveAs = (1 << 4),
				EntryRename = (1 << 5),
				EntryDelete = (1 << 6),
				EntryOpen = (1 << 7),
				EntryDefault = (1 << 8),
				EntryApply = (1 << 9)
			};

		signals:
			void sigApply();
			void sigDefault();
			void sigDelete();
			void sigEdit();
			void sigNew();
			void sigOpen();
			void sigRename();
			void sigSave();
			void sigSaveAs();
			void sigUndo();

//		private:
//			void showAction(bool b, QAction* action);

		public:
			explicit ContextMenu(QWidget* parent = nullptr);
			~ContextMenu() override;

			void registerAction(QAction* action);
			bool hasActions();

			[[nodiscard]] ContextMenuEntries entries() const;

		protected:
			void showEvent(QShowEvent* e) override;
			void languageChanged() override;
			void skinChanged() override;

		public slots:
			void showActions(ContextMenuEntries entries);
			void showAction(ContextMenu::Entry entry, bool visible);
			void showAll();

			void addPreferenceAction(PreferenceAction* action);

		private slots:
			void timedOut();
	};
}

#endif // CONTEXTMENU_H
