/* CheckableListView.h, (Created on 14.05.2024) */

/* Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SAYONARA_PLAYER_CHECKABLELISTVIEW_H
#define SAYONARA_PLAYER_CHECKABLELISTVIEW_H

#include "Utils/Pimpl.h"

#include <QListView>
#include <QList>

#include <optional>

namespace Gui
{
	struct CheckableItem
	{
		bool checked;
		QString text;
	};

	class CheckableListView :
		public QListView
	{
		Q_OBJECT
		PIMPL(CheckableListView)

		public:
			explicit CheckableListView(QWidget* parent);
			~CheckableListView() override;

			[[nodiscard]] QList<CheckableItem> items() const;

			void append(const CheckableItem& item);
			void clear();
			void init();

		protected:
			void mousePressEvent(QMouseEvent* e) override;
			void dragMoveEvent(QDragMoveEvent* e) override;
			void mouseReleaseEvent(QMouseEvent* e) override;
	};
} // Gui

#endif //SAYONARA_PLAYER_CHECKABLELISTVIEW_H
