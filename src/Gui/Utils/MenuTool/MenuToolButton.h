/* MenuToolButton.h */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MENUTOOL_H
#define MENUTOOL_H

#include "Utils/Pimpl.h"

#include "Gui/Utils/Widgets/WidgetTemplate.h"
#include "Gui/Utils/ContextMenu/ContextMenu.h"

#include <QPushButton>

namespace Gui
{
	class PreferenceAction;

	class MenuToolButton :
		public WidgetTemplate<QPushButton>
	{
		Q_OBJECT
		PIMPL(MenuToolButton)

		signals:
			void sigApply();
			void sigDefault();
			void sigDelete();
			void sigEdit();
			void sigNew();
			void sigOpen();
			void sigRename();
			void sigSave();
			void sigSaveAs();
			void sigUndo();

		public:
			explicit MenuToolButton(QWidget* parent);
			~MenuToolButton() override;

			void registerAction(QAction* action);

			void registerPreferenceAction(Gui::PreferenceAction* action);

			[[nodiscard]] Gui::ContextMenuEntries entries() const;

			void setOverrideText(bool b);

		public slots: // NOLINT(*-redundant-access-specifiers)
			void showAction(ContextMenu::Entry entry, bool visible);
			void showActions(ContextMenuEntries options);
			void showAll();

		protected:
			void languageChanged() override;
			void skinChanged() override;

		private slots:
			void mouseReleaseEvent(QMouseEvent* e) override;

		private: // NOLINT(*-redundant-access-specifiers)
			bool proveEnabled();
	};
}

#endif // MENUTOOL_H
