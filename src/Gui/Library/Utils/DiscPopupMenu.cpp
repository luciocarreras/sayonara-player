/* DiscPopupMenu.cpp */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DiscPopupMenu.h"

#include "Gui/Utils/GuiUtils.h"
#include "Gui/Utils/Icons.h"
#include "Utils/Algorithm.h"
#include "Utils/Language/Language.h"

#include <QMouseEvent>

namespace Library
{
	DiscAction::DiscAction(QWidget* parent, const Disc disc) :
		QAction(parent)
	{
		if(disc == std::numeric_limits<Disc>::max())
		{
			setText(Lang::get(Lang::All));
			setIcon(Gui::icon(Gui::Cds));
		}

		else
		{
			setText(Lang::get(Lang::Disc) + " " + QString::number(disc));
			setIcon(Gui::icon(Gui::Cd));
		}

		setData(disc);

		connect(this, &QAction::triggered, this, [this]() {
			auto ok = false;
			const auto discnumber = data().toInt(&ok);
			if(ok)
			{
				emit sigDiscPressed(discnumber);
			}
		});
	}

	DiscAction::~DiscAction() = default;

	DiscPopupMenu::DiscPopupMenu(QWidget* parent, QList<Disc> discs) :
		QMenu(parent)
	{
		Util::Algorithm::sort(discs, [](const auto disc1, const auto disc2) {
			return (disc1 < disc2);
		});

		for(int i = -1; i < discs.size(); i++)
		{
			auto* action = (i == -1)
			               ? new DiscAction(this, std::numeric_limits<Disc>::max())
			               : new DiscAction(this, discs[i]);

			addAction(action);

			connect(action, &DiscAction::sigDiscPressed, this, &DiscPopupMenu::sigDiscPressed);
		}
	}

	DiscPopupMenu::~DiscPopupMenu() = default;
}