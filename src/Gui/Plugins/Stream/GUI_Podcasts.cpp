/* GUI_Podcasts.cpp */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GUI_Podcasts.h"
#include "GUI_StationSearcher.h"
#include "ConfigurePodcastDialog.h"
#include "Gui/Plugins/ui_GUI_Podcasts.h"

#include "Components/Streaming/StationSearcher/PodcastSearcherHandler.h"
#include "Components/Streaming/Streams/PodcastHandler.h"
#include "Gui/Utils/Icons.h"
#include "Utils/Language/Language.h"

#include <QAction>

struct GUI_Podcasts::Private
{
	GUI_StationSearcher* searcher {nullptr};
	QAction* actionSearchPodcasts {nullptr};
};

GUI_Podcasts::GUI_Podcasts(Playlist::Creator* playlistCreator, PodcastHandler* podcastHandler, QWidget* parent) :
	Gui::AbstractStationPlugin(playlistCreator, podcastHandler, parent),
	m {Pimpl::make<Private>()} {}

GUI_Podcasts::~GUI_Podcasts()
{
	delete ui;
}

QString GUI_Podcasts::name() const { return "Podcasts"; }

QString GUI_Podcasts::displayName() const { return Lang::get(Lang::Podcasts); }

void GUI_Podcasts::initUi()
{
	setupParent(this, &ui);
	Gui::AbstractStationPlugin::initUi();

	m->actionSearchPodcasts = new QAction(ui->btnTool);
	ui->btnTool->registerAction(m->actionSearchPodcasts);

	connect(m->actionSearchPodcasts, &QAction::triggered, this, &GUI_Podcasts::searchPodcastTriggered);
	connect(ui->btnSearch, &QPushButton::clicked, this, &GUI_Podcasts::searchPodcastTriggered);

	retranslate();
}

QString GUI_Podcasts::titleFallbackName() const { return tr("Podcast"); }

QComboBox* GUI_Podcasts::comboStream() { return ui->comboStream; }

QPushButton* GUI_Podcasts::btnPlay() { return ui->btnListen; }

Gui::MenuToolButton* GUI_Podcasts::btnMenu() { return ui->btnTool; }

GUI_ConfigureStation* GUI_Podcasts::createConfigDialog() { return new ConfigurePodcastDialog(this); }

void GUI_Podcasts::searchPodcastTriggered()
{
	if(!m->searcher)
	{
		m->searcher = new GUI_StationSearcher(std::make_shared<PodcastSearcherHandler>(), this);
		connect(m->searcher, &GUI_StationSearcher::sigStreamSelected, this, &GUI_Podcasts::podcastSelected);
	}

	m->searcher->show();
}

void GUI_Podcasts::podcastSelected(const QString& name, const QString& url, const bool save)
{
	createStation(std::make_shared<Podcast>(name, url), !save);
}

void GUI_Podcasts::retranslate()
{
	Gui::AbstractStationPlugin::retranslate();
	ui->retranslateUi(this);

	const auto actionText = Lang::get(Lang::SearchVerb);
	if(m->actionSearchPodcasts)
	{
		m->actionSearchPodcasts->setText(actionText);
	}

	ui->btnSearch->setText(Lang::get(Lang::SearchVerb));
	ui->btnSearch->setToolTip(actionText);
}

void GUI_Podcasts::skinChanged()
{
	Gui::AbstractStationPlugin::skinChanged();

	if(m->actionSearchPodcasts)
	{
		m->actionSearchPodcasts->setIcon(Gui::icon(Gui::Search));
	}

	if(ui)
	{
		ui->btnSearch->setIcon(Gui::icon(Gui::Search));
	}
}
