/* PluginCloseButton.cpp */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PluginCloseButton.h"

#include "Gui/Utils/GuiUtils.h"
#include "Gui/Utils/Icons.h"
#include "Gui/Utils/Style.h"
#include "Utils/Language/Language.h"

#include <QEvent>

namespace
{
	QIcon createCloseIcon()
	{
		if(Style::isDark())
		{
			const auto pmNormal = Gui::pixmap(Gui::ToolDarkGrey);
			const auto pmDisabled = Gui::pixmap(Gui::ToolDisabled);

			auto icon = QIcon();
			icon.addPixmap(pmNormal, QIcon::Normal, QIcon::On);
			icon.addPixmap(pmNormal, QIcon::Normal, QIcon::Off);
			icon.addPixmap(pmDisabled, QIcon::Disabled, QIcon::On);
			icon.addPixmap(pmDisabled, QIcon::Disabled, QIcon::Off);
			icon.addPixmap(pmNormal, QIcon::Active, QIcon::On);
			icon.addPixmap(pmNormal, QIcon::Active, QIcon::Off);
			icon.addPixmap(pmNormal, QIcon::Selected, QIcon::On);
			icon.addPixmap(pmNormal, QIcon::Selected, QIcon::Off);

			return icon;
		}

		auto iconExit = Gui::icon(Gui::Exit);
		return iconExit.isNull()
		       ? Gui::icon(Gui::Close)
		       : iconExit;
	}

}

PluginCloseButton::PluginCloseButton(QWidget* parent) :
	Gui::WidgetTemplate<QPushButton>(parent)
{
	setFlat(true);
	setIconSize({14, 14}); // NOLINT(*-magic-numbers)

	setStyleSheet("margin-left: 2px; margin-right: 2px; padding-left: 0px; padding-right: 0px;");
	setToolTip(Lang::get(Lang::Close));
}

PluginCloseButton::~PluginCloseButton() = default;

void PluginCloseButton::enterEvent(QEvent* e)
{
	QPushButton::enterEvent(e);

	if(Style::isDark() && isEnabled())
	{
		const auto icon = Gui::icon(Gui::ToolGrey);
		setIcon(icon);

		e->accept();
	}
}

void PluginCloseButton::leaveEvent(QEvent* e)
{
	QPushButton::leaveEvent(e);

	setStandardIcon();
}

void PluginCloseButton::setStandardIcon()
{
	const auto icon = createCloseIcon();

	setIcon(icon);
	update();
}

void PluginCloseButton::skinChanged()
{
	setStandardIcon();
}

