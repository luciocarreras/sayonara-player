/* GUI_StyleSettings.h */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STYLESETTINGS_H
#define STYLESETTINGS_H

#include "Utils/Pimpl.h"
#include "Gui/Utils/GuiClass.h"
#include "Gui/Utils/Widgets/Dialog.h"

UI_FWD(GUI_Style)

class QSpinBox;
struct RawColorStyle;
class GUI_StyleSettings :
	public Gui::Dialog
{
	Q_OBJECT
	PIMPL(GUI_StyleSettings)
	UI_CLASS_SHARED_PTR(GUI_Style)

	signals:
		void sigStyleChanged();
		void sigStyleApplied();

	public:
		explicit GUI_StyleSettings(QWidget* parent = nullptr);
		~GUI_StyleSettings() override;

		RawColorStyle createStyleFromUi();

	public slots: // NOLINT(*-redundant-access-specifiers)
		void show(int);

	private slots:
		void stylesChanged();
		void comboStylesChanged(int index);
		void textEdited(const QString& text);
		void color1Activated();
		void color2Activated();
		void color3Activated();
		void color4Activated();
		void savePressed();
		void deletePressed();
		void undoPressed();
		void okClicked();

	private: // NOLINT(*-redundant-access-specifiers)
		void initCombobox();
		bool saveCurrent();

		void colorActivated(int index);
		void setSomethingChanged(bool b);
		bool askForSaveSettings();
		void applyStyleToUi(const RawColorStyle& style);

	protected:
		void closeEvent(QCloseEvent* event) override;
		void languageChanged() override;
		void skinChanged() override;
};

#endif // STYLESETTINGS_H
