/* GUI_LevelPainter.h */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GUI_LEVELPAINTER_H
#define GUI_LEVELPAINTER_H

#include "VisualPlugin.h"
#include "Interfaces/Engine/AudioDataReceiver.h"
#include "Utils/Pimpl.h"

class LevelDataProvider;

UI_FWD(GUI_LevelPainter)

class GUI_LevelPainter :
	public VisualPlugin,
	public Engine::LevelDataReceiver
{
	Q_OBJECT
	UI_CLASS(GUI_LevelPainter)
	PIMPL(GUI_LevelPainter)

	public:
		GUI_LevelPainter(LevelDataProvider* dataProvider, PlayManager* playManager, QWidget* parent = nullptr);
		~GUI_LevelPainter() override;

		[[nodiscard]] QString name() const override;
		[[nodiscard]] QString displayName() const override;
		[[nodiscard]] bool isActive() const override;

	protected:
		void initUi() override;
		void retranslate() override;

		[[nodiscard]] QWidget* widget() override;
		[[nodiscard]] bool hasSmallButtons() const override;
		void finalizeInitialization() override;

		[[nodiscard]] ColorStyle extractStyle(const RawColorStyle& rawColorStyle) const override;
		bool applyStyle(const ColorStyle& colorStyle) override;

		[[nodiscard]] int loadStyleIndex() const override;
		void saveStyleIndex(int index) const override;
		void paint() override;

		[[nodiscard]] bool isFadeoutCompleted() const override;
		void notifyDataProvider(bool isActive) override;

	protected slots: // NOLINT(*-redundant-access-specifiers)
		void doFadeoutStep() override;
		void setLevel(float left, float right) override;

	private:
		void reload();
};

#endif // GUI_LEVELPAINTER_H
