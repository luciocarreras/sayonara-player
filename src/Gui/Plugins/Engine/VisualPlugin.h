/* VisualPlugin.h */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENGINEPLUGIN_H
#define ENGINEPLUGIN_H

#include "GUI_StyleSettings.h"
#include "Gui/Plugins/PlayerPluginBase.h"

#include "Components/Visualization/VisualStyleTypes.h"
#include "Utils/Pimpl.h"

#include <QTimer>
#include <QPushButton>

class EngineHandler;
class PlayManager;
class VisualColorStyleChooser;

class VisualPlugin :
	public PlayerPlugin::Base
{
	Q_OBJECT
	PIMPL(VisualPlugin)

	public:
		explicit VisualPlugin(PlayManager* playManager, QWidget* parent = nullptr);
		~VisualPlugin() override;

		[[nodiscard]] bool hasTitle() const override;

	public slots: // NOLINT(*-redundant-access-specifiers)
		void initUi() override;

	protected:
		[[nodiscard]] bool isProcessing() const;
		[[nodiscard]] const ColorStyle& currentStyle() const;

		[[nodiscard]] virtual QWidget* widget() = 0;
		[[nodiscard]] virtual bool hasSmallButtons() const = 0;

		virtual bool applyStyle(const ColorStyle& style) = 0;
		[[nodiscard]] virtual ColorStyle extractStyle(const RawColorStyle& rawColorStyle) const = 0;

		[[nodiscard]] virtual int loadStyleIndex() const = 0;
		virtual void saveStyleIndex(int index) const = 0;

		virtual void paint() = 0;

		[[nodiscard]] virtual bool isFadeoutCompleted() const = 0;
		virtual void doFadeoutStep() = 0;

		virtual void notifyDataProvider(bool isActive) = 0;

		void paintEvent(QPaintEvent* e) override;
		void showEvent(QShowEvent* e) override;
		void closeEvent(QCloseEvent* e) override;
		void resizeEvent(QResizeEvent* e) override;
		void mousePressEvent(QMouseEvent* e) override;
		void enterEvent(QEvent* e) override;
		void leaveEvent(QEvent* e) override;

	private:
		void initButtons();
		void showStyleSettings();
		void reloadStyle(int newIndex);
		void setButtonSizes();
		void setButtonsVisible(bool b);

	private slots: // NOLINT(*-redundant-access-specifiers)
		void styleApplied();
		void styleChanged();
		void fadeoutTimerTimedOut();
		void configClicked();
		void nextClicked();
		void prevClicked();
		void playstateChanged(PlayState playState);
};

#endif // ENGINEPLUGIN_H


