/* PodcastSearcherHandler.cpp, (Created on 29.05.2024) */

/* Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PodcastSearcherHandler.h"
#include "CastosPodcastSearcher.h"

PodcastSearcherHandler::~PodcastSearcherHandler() = default;

QList<StationSearcherPtr> PodcastSearcherHandler::stationSearchers() const
{
	return QList<StationSearcherPtr>() << new CastosPodcastSearcher(nullptr);
}

QString PodcastSearcherHandler::loadCurrentStationSearcher() const { return {}; }

void PodcastSearcherHandler::setCurrentStationSearcher(const QString& /*name*/) {}

StationSearcher::Mode PodcastSearcherHandler::loadStationSearcherMode() const
{
	return StationSearcher::Mode::ByName;
}

void PodcastSearcherHandler::setStationSearcherMode(const StationSearcher::Mode /*mode*/) {}

QMap<StationSearcher::Mode, Lang::Term> PodcastSearcherHandler::stationSearcherModes() const
{
	return {
		{StationSearcher::Mode::ByName, Lang::Name}
	};
}

QString PodcastSearcherHandler::identifier() const { return Lang::get(Lang::Podcasts); }



