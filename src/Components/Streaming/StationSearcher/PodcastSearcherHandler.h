/* PodcastSearcherHandler.h, (Created on 29.05.2024) */

/* Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SAYONARA_PLAYER_PODCASTSEARCHERHANDLER_H
#define SAYONARA_PLAYER_PODCASTSEARCHERHANDLER_H

#include "StationSearcherHandler.h"

class PodcastSearcherHandler :
	public StationSearcherHandler
{
	public:
		~PodcastSearcherHandler() override;

		[[nodiscard]] QList<StationSearcherPtr> stationSearchers() const override;
		[[nodiscard]] QString loadCurrentStationSearcher() const override;
		void setCurrentStationSearcher(const QString& name) override;

		[[nodiscard]] QMap<StationSearcher::Mode, Lang::Term> stationSearcherModes() const override;
		[[nodiscard]] StationSearcher::Mode loadStationSearcherMode() const override;
		void setStationSearcherMode(StationSearcher::Mode mode) override;

		QString identifier() const override;
};

#endif //SAYONARA_PLAYER_PODCASTSEARCHERHANDLER_H
