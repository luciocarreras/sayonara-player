/* StationSearcherHandler.h, (Created on 28.05.2024) */

/* Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SAYONARA_PLAYER_STATIONSEARCHERHANDLER_H
#define SAYONARA_PLAYER_STATIONSEARCHERHANDLER_H

#include "StationSearcher.h"
#include "Utils/Language/Language.h"

#include <QList>
#include <QMap>

class StationSearcherHandler
{
	public:
		virtual ~StationSearcherHandler() = default;

		[[nodiscard]] virtual QString identifier() const = 0;

		[[nodiscard]] virtual QList<StationSearcherPtr> stationSearchers() const = 0;
		[[nodiscard]] virtual QString loadCurrentStationSearcher() const = 0;
		virtual void setCurrentStationSearcher(const QString& name) = 0;

		[[nodiscard]] virtual QMap<StationSearcher::Mode, Lang::Term> stationSearcherModes() const = 0;
		[[nodiscard]] virtual StationSearcher::Mode loadStationSearcherMode() const = 0;
		virtual void setStationSearcherMode(StationSearcher::Mode mode) = 0;
};

using StationSearcherHandlerPtr = std::shared_ptr<StationSearcherHandler>;

#endif //SAYONARA_PLAYER_STATIONSEARCHERHANDLER_H
