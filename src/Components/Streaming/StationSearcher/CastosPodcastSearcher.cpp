/* CastosPodcastSearcher.cpp, (Created on 28.05.2024) */

/* Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "CastosPodcastSearcher.h"
#include "CastosPodcastParser.h"
#include "RadioStation.h"

#include "Utils/Logger/Logger.h"

#include <QByteArray>
#include <QList>
#include <QMap>

CastosPodcastSearcher::CastosPodcastSearcher(QObject* parent) :
	StationSearcher(parent) {}

CastosPodcastSearcher::~CastosPodcastSearcher() = default;

QString CastosPodcastSearcher::serviceName() const { return "castos.com"; }

QString CastosPodcastSearcher::buildUrl(const QString& /*searchtext*/, const int /*serverIndex*/,
                                        const StationSearcher::Mode /*mode*/,
                                        const int /*page*/,
                                        const int /*maxEntries*/) const
{
	return "https://castos.com/wp-admin/admin-ajax.php";
}

QByteArray CastosPodcastSearcher::postData(const QString& searchtext) const
{
	const auto dataList = QStringList {
		"--sayonara",
		R"(Content-Disposition: form-data; name="search")",
		"",
		searchtext,
		"--sayonara",
		R"(Content-Disposition: form-data; name="action")",
		"",
		"feed_url_lookup_search",
		"--sayonara--"
	};

	return dataList.join("\n").toLocal8Bit() + "\n";
}

QMap<QByteArray, QByteArray> CastosPodcastSearcher::headers() const
{
	return {
		{"accept",       "*/*"},
		{"content-type", "multipart/form-data; boundary=sayonara"},
		{"origin",       "https://castos.com"},
		{"referer",      "https://castos.com/tools/find-podcast-rss-feed/"}
	};
}

std::unique_ptr<StationParser> CastosPodcastSearcher::createStationParser()
{
	return std::make_unique<CastosPodcastParser>();
}
