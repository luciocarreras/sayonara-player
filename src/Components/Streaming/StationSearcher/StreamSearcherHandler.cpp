/* StreamSearcherHandler.cpp, (Created on 29.05.2024) */

/* Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "StreamSearcherHandler.h"
#include "FMStreamSearcher.h"
#include "RadioBrowserSearcher.h"
#include "CastosPodcastSearcher.h"
#include "Utils/Settings/Settings.h"

StreamSearcherHandler::~StreamSearcherHandler() = default;

QList<StationSearcherPtr> StreamSearcherHandler::stationSearchers() const
{
	return {
		new FMStreamSearcher(nullptr),
		new RadioBrowserSearcher(nullptr)
	};
}

QString StreamSearcherHandler::loadCurrentStationSearcher() const
{
	return GetSetting(Set::Stream_RadioSearcher);
}

void StreamSearcherHandler::setCurrentStationSearcher(const QString& name)
{
	SetSetting(Set::Stream_RadioSearcher, name);
}

StationSearcher::Mode StreamSearcherHandler::loadStationSearcherMode() const
{
	return static_cast<StationSearcher::Mode>(GetSetting(Set::Stream_RadioSearcherType));
}

void StreamSearcherHandler::setStationSearcherMode(const StationSearcher::Mode mode)
{
	SetSetting(Set::Stream_RadioSearcherType, static_cast<int>(mode));
}

QMap<StationSearcher::Mode, Lang::Term> StreamSearcherHandler::stationSearcherModes() const
{
	return {
		{StationSearcher::Mode::ByName,  Lang::Name},
		{StationSearcher::Mode::ByStyle, Lang::Genre}
	};
}

QString StreamSearcherHandler::identifier() const { return Lang::get(Lang::RadioStation); }
