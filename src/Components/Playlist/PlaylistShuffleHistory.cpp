/* PlaylistShuffleBehavior.cpp */
/*
 * Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PlaylistShuffleHistory.h"
#include "Playlist.h"
#include "PlaylistModifiers.h"

#include "Utils/Algorithm.h"
#include "Utils/Utils.h"
#include "Utils/MetaData/MetaData.h"
#include "Utils/MetaData/MetaDataList.h"

#include <QList>

namespace Playlist
{
	namespace
	{
		QList<int> getUnplayedTrackIndexes(Playlist* playlist, const QList<UniqueId>& shuffleHistory)
		{
			const auto& tracks = playlist->tracks();
			auto trackIndex = 0;
			auto unplayedTracks = QList<int>();
			for(const auto& track: tracks)
			{
				const auto uniqueId = track.uniqueId();
				if(!shuffleHistory.contains(uniqueId))
				{
					unplayedTracks << trackIndex;
				}

				trackIndex++;
			}

			return unplayedTracks;
		}
	}

	struct ShuffleHistory::Private
	{
		QList<UniqueId> shuffleHistory;
		Playlist* playlist;

		explicit Private(Playlist* playlist) :
			playlist {playlist} {}
	};

	ShuffleHistory::ShuffleHistory(Playlist* playlist)
	{
		m = Pimpl::make<Private>(playlist);
	}

	ShuffleHistory::~ShuffleHistory() = default;

	void ShuffleHistory::addTrack(const UniqueId uniqueId)
	{
		m->shuffleHistory << uniqueId;
	}

	int ShuffleHistory::previousShuffleIndex() const
	{
		for(auto historyIndex = m->shuffleHistory.size() - 2; historyIndex >= 0; historyIndex--)
		{
			const auto uniqueId = m->shuffleHistory[historyIndex];
			const auto index = Util::Algorithm::indexOf(m->playlist->tracks(), [&](const auto& track) {
				return (uniqueId == track.uniqueId());
			});

			if(index >= 0)
			{
				return index;
			}
		}

		return -1;
	}

	int ShuffleHistory::nextTrackIndex(bool isRepeatAllEnabled) const
	{
		auto unplayedTracks = getUnplayedTrackIndexes(m->playlist, m->shuffleHistory);
		if(const auto allShuffleTracksPlayed = unplayedTracks.isEmpty(); allShuffleTracksPlayed)
		{
			if(!isRepeatAllEnabled)
			{
				return -1;
			}

			m->shuffleHistory.clear();
		}

		const auto count = m->playlist->count();
		if(count < 2)
		{ // for zero entries, there's no value index. For 1 entry, we use 0
			return count - 1;
		}

		if(m->shuffleHistory.isEmpty())
		{
			return Util::randomNumber(1, count - 1);
		}

		const auto index = Util::randomNumber(0, unplayedTracks.count() - 1);
		return unplayedTracks[index];
	}

	void ShuffleHistory::replaceTrack(const MetaData& oldTrack, const MetaData& newTrack)
	{
		auto i = 0;
		for(const auto& uniqueId: qAsConst(m->shuffleHistory))
		{
			if(uniqueId == oldTrack.uniqueId())
			{
				m->shuffleHistory.replace(i, newTrack.uniqueId());
			}

			i++;
		}
	}

	void ShuffleHistory::popBack()
	{
		if(m->shuffleHistory.count() > 0)
		{
			m->shuffleHistory.pop_back();
		}
	}

	void ShuffleHistory::clear()
	{
		m->shuffleHistory.clear();
	}
}