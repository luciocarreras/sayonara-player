/* VisualStyleTypes.h */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STYLETYPES_H
#define STYLETYPES_H

#include <QColor>
#include <QString>
#include <QHash>

#include <vector>

struct ColorList
{
	QString name;
	QList<QColor> colors;
};

struct ColorStyle
{
	using ColorInfo = std::vector<QHash<int, QColor>>;
	QString name;

	// list size is number or rectangles
	// int is the step index
	ColorInfo style;
	ColorList colorList;

	int rectHeight {-1};
	int rectWidth {-1};

	int rectCount {-1};
	int fadingSteps {-1};
	int horSpacing {-1};
	int verSpacing {-1};

	[[nodiscard]] bool isValid() const;
};

struct RawColorStyle
{
	ColorList colorList;

	int spectrumBins {20};

	int rectHeightSpectrum {2};
	int rectWidthLevel {3};
	int rectHeightLevel {6};
	int levelRects {0};

	int fadingStepsSpectrum {20};
	int fadingStepsLevel {20};

	int horSpacingLevel {2};
	int verSpacingLevel {2};

	int horSpacingSpectrum {2};
	int verSpacingSpectrum {1};

	[[nodiscard]] QString toString() const;
};

namespace Util
{
	[[nodiscard]] RawColorStyle
	convertToRawColorStyle(const ColorStyle& levelStyle, const ColorStyle& spectrumStyle);

	[[nodiscard]] ColorStyle createSpectrumStyle(const RawColorStyle& rawColorStyle, int widgetHeight);
	[[nodiscard]] ColorStyle createLevelStyle(const RawColorStyle& rawColorStyle, int widgetWidth);

	[[nodiscard]] RawColorStyle createRgbStyle();
	[[nodiscard]] RawColorStyle createBwStyle();
}

#endif // STYLETYPES_H
