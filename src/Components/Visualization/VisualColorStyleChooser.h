/* VisualColorStyleChooser.h */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENGINECOLORSTYLECHOOSER_H
#define ENGINECOLORSTYLECHOOSER_H

#include <QObject>
#include <QList>
#include <QHash>
#include <QColor>

#include "Utils/Pimpl.h"

struct RawColorStyle;
class VisualColorStyleChooser :
	public QObject
{
	Q_OBJECT
	PIMPL(VisualColorStyleChooser)

	public:
		VisualColorStyleChooser();
		~VisualColorStyleChooser() noexcept override;

		[[nodiscard]] RawColorStyle style(int index) const;
		[[nodiscard]] int count() const;

		void reload();
};

#endif // ENGINECOLORSTYLECHOOSER_H
