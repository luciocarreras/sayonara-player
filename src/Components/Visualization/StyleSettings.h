/* StyleSettings.h, (Created on 10.11.2024) */

/* Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SAYONARA_PLAYER_STYLESETTINGS_H
#define SAYONARA_PLAYER_STYLESETTINGS_H

#include "Utils/Pimpl.h"

#include <QList>
#include <QObject>

#include <optional>

struct RawColorStyle;
class StyleSettings :
	public QObject
{
	Q_OBJECT
	PIMPL(StyleSettings)

	signals:
		void sigStylesChanged();

	public:
		explicit StyleSettings(QObject* parent);
		~StyleSettings() override;

		[[nodiscard]] QList<RawColorStyle> styles() const;
		[[nodiscard]] std::optional<RawColorStyle> currentStyle() const;

		[[nodiscard]] int count() const;
		void select(int index);
		[[nodiscard]] int currentIndex() const;
		[[nodiscard]] QString nameProposal() const;

		bool saveCurrent(const RawColorStyle& style);
		bool deleteCurrent();

	private:
		[[nodiscard]] std::optional<RawColorStyle> style(int index) const;

};

#endif //SAYONARA_PLAYER_STYLESETTINGS_H
