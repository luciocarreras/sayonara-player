/* PipelineFactory.cpp, (Created on 15.01.2025) */

/* Copyright (C) 2011-2025 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PipelineFactory.h"
#include "Pipeline.h"

namespace Engine
{
	PipelineFactory::~PipelineFactory() noexcept = default;

	StandardPipelineFactory::~StandardPipelineFactory() noexcept = default;

	std::shared_ptr<Pipeline> StandardPipelineFactory::createPipeline(const QString& name) const
	{
		return Pipeline::create(name, nullptr);
	}
} // Engine