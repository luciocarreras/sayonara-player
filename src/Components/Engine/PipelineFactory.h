/* PipelineFactory.h, (Created on 15.01.2025) */

/* Copyright (C) 2011-2025 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SAYONARA_PLAYER_PIPELINEFACTORY_H
#define SAYONARA_PLAYER_PIPELINEFACTORY_H

#include <memory>

class QString;
namespace Engine
{
	class Pipeline;

	class PipelineFactory
	{
		public:
			virtual ~PipelineFactory() noexcept;
			[[nodiscard]] virtual std::shared_ptr<Pipeline> createPipeline(const QString& name) const = 0;

	};

	class StandardPipelineFactory :
		public PipelineFactory
	{
		public:
			~StandardPipelineFactory() noexcept override;

			[[nodiscard]] std::shared_ptr<Pipeline> createPipeline(const QString& name) const override;
	};

	using PipelineFactoryPtr = std::shared_ptr<PipelineFactory>;

} // Engine

#endif //SAYONARA_PLAYER_PIPELINEFACTORY_H
