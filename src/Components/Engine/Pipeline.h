/* PlaybackPipeline.h */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SAYONARA_PLAYER_PIPELINE_H
#define SAYONARA_PLAYER_PIPELINE_H

#include "PipelineExtensions/StreamRecordable.h"
#include "Utils/typedefs.h"
#include <gst/gst.h>
#include <memory>

#include <QObject>

namespace Engine
{
	class Engine;

	class Pipeline :
		public QObject,
		public PipelineExtensions::StreamRecordable
	{
		Q_OBJECT
		signals:
			void sigAboutToFinishMs(MilliSeconds ms);
			void sigPositionChangedMs(MilliSeconds ms);
			void sigDataAvailable(const QByteArray& data);

		public:
			~Pipeline() override;

			virtual bool init(Engine* engine) = 0;
			virtual bool prepare(const QString& uri, const QString& userAgent = QString()) = 0;

			[[nodiscard]] virtual bool hasElement(GstElement* e) const = 0;
			virtual void checkPosition() = 0;

			[[nodiscard]] virtual GstState state() const = 0;

			virtual void setVisualizerEnabled(bool isLevelActive, bool isSpectrumActive) = 0;
			[[nodiscard]] virtual bool isLevelVisualizerEnabled() const = 0;
			[[nodiscard]] virtual bool isSpectrumVisualizerEnabled() const = 0;

			virtual void setBroadcastingEnabled(bool b) = 0;
			[[nodiscard]] virtual bool isBroadcastingEnabled() const = 0;

			virtual void fadeIn() = 0;
			virtual void fadeOut() = 0;

			virtual void startDelayedPlayback(MilliSeconds ms) = 0;

			virtual void seekRelative(double percent, MilliSeconds duration) = 0;
			virtual void seekAbsoluteMs(MilliSeconds ms) = 0;
			virtual void seekRelativeMs(MilliSeconds ms) = 0;
			[[nodiscard]] virtual MilliSeconds duration() const = 0;
			[[nodiscard]] virtual MilliSeconds timeToGo() const = 0;

			virtual void setEqualizerBand(int band, int value) = 0;

			static std::shared_ptr<Pipeline> create(const QString& name, QObject* parent = nullptr);

		public slots: // NOLINT(readability-redundant-access-specifiers)
			virtual void play() = 0;
			virtual void stop() = 0;
			virtual void pause() = 0;

		protected:
			explicit Pipeline(QObject* parent);
	};
} // SAYONARA_PLAYER_PIPELINE_H

#endif
