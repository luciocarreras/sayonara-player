/* LyricServerMap.cpp, (Created on 15.05.2024) */

/* Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LyricServerEntry.h"

#include "Utils/Algorithm.h"

#include <QStringList>
#include <utility>

namespace Lyrics
{
	ServerEntry::ServerEntry() = default;

	ServerEntry::ServerEntry(QString name_, const bool isChecked_) :
		name {std::move(name_)},
		isChecked {isChecked_} {}

	ServerEntry::ServerEntry(const ServerEntry& other) :
		name {other.name},
		isChecked {other.isChecked} {}

	ServerEntry& ServerEntry::operator=(const ServerEntry& other)
	{
		name = other.name;
		isChecked = other.isChecked;

		return *this;
	}

	QString ServerEntry::toString() const
	{
		return QString("%1:%2")
			.arg(name)
			.arg(static_cast<int>(isChecked));
	}

	bool ServerEntry::loadFromString(const QString& str)
	{
		if(const auto splitted = str.split(":"); splitted.count() == 2)
		{
			name = splitted[0];

			bool ok;
			isChecked = splitted[1].toInt(&ok);

			return ok;
		}

		return false;
	}

	bool ServerEntry::operator==(const ServerEntry& other) const
	{
		return (isChecked == other.isChecked) &&
		       (name == other.name);
	}
}
