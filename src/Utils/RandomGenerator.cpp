/* RandomGenerator.cpp */

/* Copyright (C) 2011-2024 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Utils/RandomGenerator.h"
#include <random>
#include <chrono>

struct RandomGenerator::Private // NOLINT(*-msc51-cpp)
{
	std::mt19937 generator;
};

RandomGenerator::RandomGenerator() :
	m {Pimpl::make<RandomGenerator::Private>()}
{
	updateSeed();
}

RandomGenerator::~RandomGenerator() = default;

void RandomGenerator::updateSeed()
{
	m->generator = getGenerator();
}

int RandomGenerator::getNumber(const int min, const int max)
{
	auto distribution = std::uniform_int_distribution<int>(min, max);
	return distribution(m->generator);
}

int RandomGenerator::getRandomNumber(const int min, const int max)
{
	static RandomGenerator generator;
	return generator.getNumber(min, max);
}

std::mt19937 RandomGenerator::getGenerator()
{
	const auto now = std::chrono::system_clock::now();
	const auto seed = now.time_since_epoch().count();
	return std::mt19937(seed);
}

