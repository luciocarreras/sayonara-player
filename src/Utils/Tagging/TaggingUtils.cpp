/* ${CLASS_NAME}.h */
/*
 * Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TaggingUtils.h"

#include <QString>

#include <taglib/tstring.h>
#include <taglib/xiphcomment.h>
#include <taglib/mp4tag.h>
#include <taglib/id3v2tag.h>
#include <taglib/mpegfile.h>
#include <taglib/oggflacfile.h>
#include <taglib/flacfile.h>
#include <taglib/mp4file.h>
#include <taglib/wavfile.h>

namespace
{
	constexpr const auto InvalidParsedTag = Tagging::ParsedTag {nullptr, Tagging::TagType::Unsupported};

	template<typename T>
	bool isValid(TagLib::Tag* tag)
	{
		return (dynamic_cast<T*>(tag) != nullptr);
	}

	Tagging::TagType getTagTypeFromTag(TagLib::Tag* tag)
	{
		if(isValid<TagLib::ID3v2::Tag>(tag)) { return Tagging::TagType::ID3v2; }

		if(isValid<TagLib::ID3v1::Tag>(tag)) { return Tagging::TagType::ID3v1; }

		if(isValid<TagLib::Ogg::XiphComment>(tag)) { return Tagging::TagType::Xiph; }

		if(isValid<TagLib::MP4::Tag>(tag)) { return Tagging::TagType::MP4; }

		return Tagging::TagType::Unsupported;
	}

	Tagging::ParsedTag getParsedTagFromMpeg(TagLib::MPEG::File* file)
	{
		if(file && file->hasID3v2Tag())
		{
			return {file->ID3v2Tag(), Tagging::TagType::ID3v2};
		}

		if(file && file->hasID3v1Tag())
		{
			return {file->ID3v1Tag(), Tagging::TagType::ID3v1};
		}

		if(file)
		{
			return {file->ID3v2Tag(true), Tagging::TagType::ID3v2};
		}

		return InvalidParsedTag;
	}

	Tagging::ParsedTag getParsedTagFromFlac(TagLib::FLAC::File* file)
	{
		if(file && file->hasXiphComment())
		{
			return {file->xiphComment(), Tagging::TagType::Xiph};
		}

		if(file && file->hasID3v2Tag())
		{
			return {file->ID3v2Tag(), Tagging::TagType::ID3v2};
		}

		if(file && file->hasID3v1Tag())
		{
			return {file->ID3v1Tag(), Tagging::TagType::ID3v1};
		}

		if(file && file->tag())
		{
			return {file->tag(), Tagging::TagType::Unknown};
		}

		if(file && !file->tag())
		{
			return {file->ID3v2Tag(true), Tagging::TagType::ID3v2};
		}

		return InvalidParsedTag;
	}

	Tagging::ParsedTag getParsedTagFromMP4(TagLib::MP4::File* file)
	{
		return (file && file->hasMP4Tag())
		       ? Tagging::ParsedTag {file->tag(), Tagging::TagType::MP4}
		       : InvalidParsedTag;
	}

	Tagging::ParsedTag getParsedTagFromWAV(TagLib::RIFF::WAV::File* file)
	{
		if(file && file->hasID3v2Tag())
		{
			return {file->ID3v2Tag(), Tagging::TagType::ID3v2};
		}

		if(file && file->hasInfoTag())
		{
			return {file->InfoTag(), Tagging::TagType::Unknown};
		}

		return InvalidParsedTag;
	}
}

namespace Tagging
{
	TagLib::MP4::Tag* ParsedTag::mp4Tag() const
	{
		return dynamic_cast<TagLib::MP4::Tag*>(this->tag);
	}

	TagLib::ID3v2::Tag* ParsedTag::id3Tag() const
	{
		return dynamic_cast<TagLib::ID3v2::Tag*>(this->tag);
	}

	TagLib::Ogg::XiphComment* ParsedTag::xiphTag() const
	{
		return dynamic_cast<TagLib::Ogg::XiphComment*>(this->tag);
	}

	TagLib::String convertString(const QString& str)
	{
		return {str.toUtf8().data(), TagLib::String::Type::UTF8};
	}

	QString convertString(const TagLib::String& str)
	{
		return {str.toCString(true)};
	}

	ParsedTag getParsedTagFromFileRef(const TagLib::FileRef& fileRef)
	{
		if(auto* mpg = dynamic_cast<TagLib::MPEG::File*>(fileRef.file()); mpg)
		{
			return getParsedTagFromMpeg(mpg);
		}

		if(auto* flac = dynamic_cast<TagLib::FLAC::File*>(fileRef.file()); flac)
		{
			return getParsedTagFromFlac(flac);
		}

		if(auto* mp4 = dynamic_cast<TagLib::MP4::File*>(fileRef.file()); mp4)
		{
			return getParsedTagFromMP4(mp4);
		}

		if(auto* wavFile = dynamic_cast<TagLib::RIFF::WAV::File*>(fileRef.file()); wavFile)
		{
			return getParsedTagFromWAV(wavFile);
		}

		if(fileRef.file())
		{
			const auto tagType = getTagTypeFromTag(fileRef.tag());
			return {
				fileRef.tag(),
				(tagType == Tagging::TagType::Unsupported) ? Tagging::TagType::Unknown : tagType
			};
		}

		return InvalidParsedTag;
	}

	Tagging::TagType getTagType(const QString& filepath)
	{
		auto fileRef = TagLib::FileRef(TagLib::FileName(filepath.toUtf8()));
		return (isValidFile(fileRef))
		       ? getParsedTagFromFileRef(fileRef).type
		       : TagType::Unknown;
	}

	QString tagTypeToString(Tagging::TagType type)
	{
		switch(type)
		{
			case Tagging::TagType::ID3v1:
				return "ID3v1";
			case Tagging::TagType::ID3v2:
				return "ID3v2";
			case Tagging::TagType::Xiph:
				return "Xiph";
			case Tagging::TagType::MP4:
				return "MP4";
			case Tagging::TagType::Unknown:
				return "Unknown";
			default:
				return "Partially unsupported";
		}
	}

	bool isValidFile(const TagLib::FileRef& fileRef)
	{
		return (!fileRef.isNull() && fileRef.tag() && fileRef.file() && fileRef.file()->isValid());
	}
}