# Issue
For a feature request, please use the "Feature Request" template

## Version
e.g. 1.10.0-stable1

## Where did you get Sayonara from?
PPA, Fedora repos, Appimage ...

## Desktop environment
E.g. Ubuntu 22.04, Manjaro Linux KDE

## What's not working

## What do you expect?

## How to reproduce?

## Did this ever work? If yes, which version was the last version without the issue?
