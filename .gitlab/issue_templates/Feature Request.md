# Feature request
If you want to report a bug/issue please use the "Default" template

[ ] I have read the [FAQ regarding feature requests](https://sayonara-player.com/faq/#i-want-to-have-a-new-special-feature-can-you-integrate-it)

# Description

Your description here
