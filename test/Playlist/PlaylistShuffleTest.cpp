/*
 * Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PlaylistTestUtils.h"
#include "Common/FileSystemMock.h"
#include "Common/PlayManagerMock.h"
#include "Common/SayonaraTest.h"

#include "Components/Playlist/Playlist.h"
#include "Components/Playlist/PlaylistModifiers.h"
#include "Utils/FileSystem.h"
#include "Utils/MetaData/MetaData.h"
#include "Utils/MetaData/MetaDataList.h"
#include "Utils/Playlist/PlaylistMode.h"
#include "Utils/Settings/Settings.h"

// access working directory with Test::Base::tempPath("somefile.txt");

namespace
{
	Util::FileSystemPtr allAvailableFileSystem()
	{
		return std::make_shared<Test::AllFilesAvailableFileSystem>();
	}

	PlaylistPtr createShufflePlaylist(const bool withRepeatAll, const int numTracks)
	{
		SetSetting(Set::PL_StartPlaying, false);

		const auto tracks = Test::Playlist::createTrackList(0, numTracks);
		auto playlist = std::make_shared<Playlist::Playlist>(
			1, "Hallo", new Test::PlayManagerMock(), allAvailableFileSystem());

		Playlist::Mode mode;
		mode.setShuffle(true);
		mode.setRepAll(withRepeatAll);

		playlist->setMode(mode);
		playlist->createPlaylist(tracks);

		return playlist;
	}

	std::pair<PlaylistPtr, QList<int>> prepareRepeatTest(const bool withRepeatAll, const int numTracks = 10)
	{
		auto playlist = createShufflePlaylist(withRepeatAll, numTracks);

		auto playedTracks = QList<int> {};
		for(auto i = 0; i < playlist->tracks().count(); i++)
		{
			playlist->next();
			playedTracks << playlist->findCurrentTrackIndex();
		}

		return {playlist, playedTracks};
	}

}

class PlaylistShuffleTest :
	public Test::Base
{
	Q_OBJECT

	public:
		PlaylistShuffleTest() :
			Test::Base("PlaylistShuffleTest") {}

	private slots:

		// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
		[[maybe_unused]] void testShuffleFirstTrack()
		{
			auto playlist = createShufflePlaylist(false, 2); // NOLINT(*-magic-numbers)
			playlist->play();

			const auto currentIndex = playlist->findCurrentTrackIndex();
			QVERIFY(currentIndex > 0);
		}

		// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
		[[maybe_unused]] void testShuffleOneTrack()
		{
			auto playlist = createShufflePlaylist(false, 1);
			playlist->play();

			const auto currentIndex = playlist->findCurrentTrackIndex();
			QVERIFY(currentIndex == 0);
		}

		// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
		[[maybe_unused]] void testUntilEndOfTracks()
		{
			auto playlist = createShufflePlaylist(false, 10); // NOLINT(*-magic-numbers)
			const auto& tracks = playlist->tracks();

			QList<int> playedTracks;
			QList<int> incrementedTracks;
			for(auto i = 0; i < tracks.count(); i++)
			{
				playlist->next();
				playedTracks << playlist->findCurrentTrackIndex();
			}
			QVERIFY(playedTracks.count() == tracks.count());
			QVERIFY(playlist->findCurrentTrackIndex() != -1);

			for(auto i = 0; i < tracks.count(); i++)
			{
				incrementedTracks << i;
			}
			QVERIFY(playedTracks != incrementedTracks);
		}

		// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
		[[maybe_unused]] void testWithRepAll()
		{
			auto [playlist, _] = prepareRepeatTest(true);
			for(int i = 0; i < playlist->count() * 3; i++)
			{
				playlist->next();
				QVERIFY(playlist->findCurrentTrackIndex() >= 0);
			}
		}

		// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
		[[maybe_unused]] void testWithoutRepAll()
		{
			auto [playlist, _] = prepareRepeatTest(false);
			playlist->next();
			QVERIFY(playlist->findCurrentTrackIndex() == -1);
		}

		// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
		[[maybe_unused]] void testBackward()
		{
			auto [playlist, playedTracks] = prepareRepeatTest(false);
			const auto count = playedTracks.count();
			QVERIFY(playlist->findCurrentTrackIndex() == playedTracks.last());

			for(int i = 1; i < count; i++)
			{
				playlist->previous();
				const auto currentTrack = playlist->findCurrentTrackIndex();
				const auto expectedTrack = playedTracks[count - i - 1];
				QVERIFY(currentTrack == expectedTrack);
			}
		}

		// NOLINTNEXTLINE(readability-convert-member-functions-to-static)
		[[maybe_unused]] void testShuffleAndRepeatAllOfSingleTrack()
		{ // https://gitlab.com/luciocarreras/sayonara-player/-/issues/444
			auto playlist = createShufflePlaylist(true, 1);
			playlist->play();
			playlist->next();

			QVERIFY(playlist->findCurrentTrackIndex() == 0);
		}
};

QTEST_GUILESS_MAIN(PlaylistShuffleTest)

#include "PlaylistShuffleTest.moc"
