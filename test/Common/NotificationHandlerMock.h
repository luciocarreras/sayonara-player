/* NotificationHandlerMock.h, (Created on 10.05.2024) */

/* Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SAYONARA_PLAYER_NOTIFICATIONHANDLERMOCK_H
#define SAYONARA_PLAYER_NOTIFICATIONHANDLERMOCK_H

#include "Components/Notification/NotificationHandler.h"

namespace Test
{
	class NotificationHandlerMock :
		public NotificationHandler
	{
		public:
			void registerNotificator(Notificator* /*notificator*/) override;

			void changeCurrentNotificator(const QString& /*name*/) override;

			[[nodiscard]] QList<Notificator*> notificators() const override;

			[[nodiscard]] Notificator* currentNotificator() const override;

			void notify(const MetaData& /*track*/) override;

			void notify(const QString& /*title*/, const QString& /*message*/, const QString& /*imagePath*/) override;
	};

} // Test

#endif //SAYONARA_PLAYER_NOTIFICATIONHANDLERMOCK_H
